$(document).ready(function() {
    const date = new Date();
    document.querySelector('.year').innerHTML = date.getFullYear();

    var valueSub = document.getElementById('id-tiporeq-sub')
    var selectReq = document.getElementById('id_tipo_requisicao');
    selectReq.addEventListener('change', function() {
        if (this.value == valueSub.value) {
            var el = document.getElementById('hidden-div-req')
            el.classList.remove('hidden')
            var att = document.createAttribute('required')
            var sel = document.getElementById('id_funcionario_substituicao')
            sel.setAttributeNode(att)
        } else {
            var el = document.getElementById('hidden-div-req')
            el.className += ' hidden'
            $('#id_funcionario_substituicao').prop('selectedIndex', 0);
            document.getElementById("id_funcionario_substituicao").removeAttribute("required");
        }
    }, false);
});