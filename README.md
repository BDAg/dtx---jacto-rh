# DTX - RH Jacto

![](https://media.licdn.com/dms/image/C510BAQF4WaAeV2kUPw/company-logo_200_200/0?e=2159024400&v=beta&t=sz7DYhrt08ZaA5CZaUs8KqMTT6tGV2bmPGVIO_3-Qdc)


## Index


<!--ts-->
   * [Nossa Wiki](https://gitlab.com/BDAg/dtx---jacto-rh/wikis/home)
   * [Requisitos](#requisitos)
   * [Começando Projeto para Windows](#começando-projeto-windows)
   * [Observações](#observações)
   * [Comandos de Apoio](#comandos-de-apoio)
   * [Links](#links)
   * [Sprints](#sprints)

<!--te-->


----

## Requisitos

- Python >= 3.6
- pip
- virtualenv
- requirements.txt (all dependencies)

----

## Começando Projeto (Windows)

- Instalar python em seu computador. (https://www.python.org/)

- Após instalar o python, certifique-se que ele está nas variáveis de ambiente (PATH)

- Caso o pip não esteja instalado, baixe o mesmo -> https://bootstrap.pypa.io/get-pip.py (gerenciador de pacotes que utilizaremos do python). <br>
- Vá ate o diretório onde o arquivo **get-pip.py** foi instalado e execute o comando:

    `$ python get-pip.py`

- Clone o projeto dando o comando

`$ git clone <link repositorio>`
<br>
<br>
`$ git clone https://gitlab.com/BDAg/dtx---jacto-rh.git`

- Va ate a pasta "dtx---jacto-rh" e instale o virtualenv nela

<br>

`$ pip install virtualenv`

<br>

`$ python -m venv venv`

<br>

- Após isso é necessário ativar o virtualenv

`$ venv/Scripts/activate.bat`
<br>
Sua linha de comando ficará mais ou menos assim...
<br>
<br>
**(venv) C:\Users\\`<Usuário>`\\`<Caminho ate o projeto>`\ dtx---jacto-rh**
<br>


- Na pasta do projeto e **com o venv ativado** instale as bibliotecas no arquivo requirements.txt

`$ pip install -r requirements.txt`

- Finalmente para rodar o projeto execute o comando

`$ python manage.py runserver`

----

## Observações

Pode ser que o mysqlclient de problema na instalação, verifique se sua versão 
do Python é 3.6 para cima, se tem o Microsoft Visual C++ 14.0, e se sua máquina
for 64bits verifique se seu Python está adequado. <br>
Sobre a instalação, para o Linux é diferente. <br>
[Link de Apoio](https://medium.com/horadecodar/instalando-django-2-1-e-criando-um-projeto-windows-e-linux-67cbff58496c)

----

## Comandos de Apoio

### Criar virtualvenv
python -m venv venv ( na pasta dtx---jacto-rh)
<br>
dtx---jacto-rh <br>
|_ venv

### Instalar requirements 
pip install -r requirements.txt 

### Help do Manage.py
python manage.py help

### Gerar um App
python manage.py startapp `<nome app>`

### Run migration
python manage.py migrate (instala o admin do python, tenha certeza de ter o banco jacto na sua maquina)

### Criar superusuario 
python manage.py createsuperuser (cria usuario para o admin)

### Acessar Admin

- http://127.0.0.1:(porta)/admin 

A porta por padrão é 8000 mas você pode colocar na que preferir <br>
ex: python manage.py runserver *8080*

### Gerar arquivos staticos
python manage.py collectstatic


----

## Links

`<projetoDTX>` : <https://gitlab.com/BDAg/dtx---jacto-rh>
- https://gist.github.com/bradtraversy/0df61e9b306db3d61eb24793b6b7132d
- https://stackoverflow.com/questions/37094032/how-to-run-cloned-django-project

### Download Python 64 bits
- https://www.python.org/ftp/python/3.7.5/python-3.7.5-amd64.exe


----

## Sprints

- [x] MVP (Definição Projeto)
- [ ] FrontEnd Telas
- [ ] CRUD Telas


