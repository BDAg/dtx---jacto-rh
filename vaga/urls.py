from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='vaga'),
    path('nova-vaga/', views.novaVaga, name='nova-vaga/')
]