from django.shortcuts import render
from django.http import HttpResponse
from .models import Vaga, Tiporequisicao
from .forms import VagaForm, RawVagaForm
from django.shortcuts import redirect

def index(request):
    vagas = Vaga.objects.all()    

    return render(request, 'vaga/index.html', {'vagas': vagas})

def novaVaga(request):
    form = RawVagaForm()
    sub_id = Tiporequisicao.objects.get(descricao__iexact='substituicao')
    context = {
        'form': form,
        'sub': sub_id
    }
    if request.method == 'POST':
        form = RawVagaForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data)
            Vaga.objects.create(**form.cleaned_data)
            return redirect('/vaga')
        else:
            print(form.errors)
    else:
        return render(request, 'vaga/nova-vaga.html', context)
