from django.db import models
from pages.models import Ccusto, Cargo, Tiporequisicao, Sexo, Tipocontrato, Usuario, Funcionario

class Vaga(models.Model):
    id_vaga = models.AutoField(primary_key=True)
    horario_trabalho = models.CharField(max_length=50)
    internet = models.IntegerField()
    motorista = models.IntegerField()
    sap = models.IntegerField()
    consideracao = models.CharField(max_length=450)
    ccusto = models.ForeignKey(Ccusto, models.DO_NOTHING, db_column='ccusto')
    cargo = models.ForeignKey(Cargo, models.DO_NOTHING, db_column='cargo')
    tipo_requisicao = models.ForeignKey(Tiporequisicao, models.DO_NOTHING, db_column='tipo_requisicao')
    sexo = models.ForeignKey(Sexo, models.DO_NOTHING, db_column='sexo')
    tipo_contrato = models.ForeignKey(Tipocontrato, models.DO_NOTHING, db_column='tipo_contrato')
    funcionario_substituicao = models.ForeignKey(Funcionario, models.DO_NOTHING, blank=True, db_column='funcionario_substituicao')

    class Meta:        
        db_table = 'vaga'

class Contato(models.Model):
    id_contato = models.AutoField(primary_key=True)
    vaga = models.ForeignKey(Vaga, models.DO_NOTHING)
    usuario = models.ForeignKey(Usuario, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'contato'