from django import forms

from .models import Vaga, Tipocontrato, Tiporequisicao, Cargo, Ccusto, Sexo, Funcionario

class VagaForm(forms.ModelForm):
    class Meta:
        model = Vaga
        fields = [
            'ccusto',
            'cargo',
            'sap',
            'horario_trabalho',
            'internet',
            'motorista',
            'consideracao',
            'tipo_requisicao',
            'sexo',
            'tipo_contrato',
            'funcionario_substituicao'
        ]

class RawVagaForm(forms.Form):
    ccusto = forms.ModelChoiceField(queryset=Ccusto.objects.all(), 
            widget=forms.Select(attrs={
                                    'class':'form-control'                        
                                    }))
    
    cargo = forms.ModelChoiceField(queryset=Cargo.objects.all(), 
            widget=forms.Select(attrs={
                                    'class':'form-control'                        
                                    }))

    internet = forms.ChoiceField(choices=[('1', 'Sim'), ('0', 'Não')],
                                    required=True,
                                    widget=forms.Select(attrs={'class':'form-control'}))

    motorista = forms.ChoiceField(choices=[('1', 'Sim'), ('0', 'Não')],
                                    required=True,
                                    widget=forms.Select(attrs={'class':'form-control'}))

    sap = forms.ChoiceField(label='SAP', choices=[('1', 'Sim'), ('0', 'Não')],
                                    required=True,
                                    widget=forms.Select(attrs={'class':'form-control'}))                                    

    sexo = forms.ModelChoiceField(queryset=Sexo.objects.all(), 
            widget=forms.Select(attrs={
                                    'class':'form-control'                        
                                    }))

    tipo_requisicao = forms.ModelChoiceField(label='Tipo Requisição', queryset=Tiporequisicao.objects.all(), 
            widget=forms.Select(attrs={
                                    'class':'form-control'                        
                                    }))

    funcionario_substituicao = forms.ModelChoiceField(label='Funcionário Substituição', required=False,queryset=Funcionario.objects.all(), 
            widget=forms.Select(attrs={
                                    'class':'form-control'                        
                                    }))     

    tipo_contrato = forms.ModelChoiceField(label='Tipo Contrato', queryset=Tipocontrato.objects.all(), 
            widget=forms.Select(attrs={
                                    'class':'form-control'                        
                                    }))
                              
    horario_trabalho = forms.CharField(max_length=50, label='Horário de Trabalho', 
        widget=forms.TextInput(attrs={
                                      'placeholder':'Digite o horário de trabalho',
                                      'class':'form-control',
                                      'rows': '4'
                                      }))
    
    consideracao = forms.CharField(max_length=450, label='Considerações', 
        widget=forms.Textarea(attrs={
                                      'placeholder':'Digite aqui as considerações',
                                      'class':'form-control',
                                      'rows': '4'
                                      }))
