from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate
from django.http import HttpResponse
from .models import Usuario
from .forms import UsuarioForm


# Create your views here.
def loginForm(request):
    if request.method == "POST":
        form = UsuarioForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['email'],
                   password=cd['senha'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Authenticated successfully')
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('Invalid Login')
    else:
        form = UsuarioForm()
    return render(request, 'pages/login.html', {'form': form})

def logoutForm(request):
    logout(request)
    return render(request, 'pages/login.html')
