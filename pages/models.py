# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Tipocontrato(models.Model):
    id_tipocontrato = models.AutoField(primary_key=True)
    descricao = models.CharField(max_length=450)

    def __str__(self):
        return self.descricao

    class Meta:
        db_table = 'tipocontrato'

class Ccusto(models.Model):
    id_ccusto = models.AutoField(primary_key=True)
    descricao = models.CharField(max_length=450)
    
    def __str__(self):
        return self.descricao
    
    class Meta:
        db_table = 'ccusto'

class Cargo(models.Model):
    id_cargo = models.AutoField(primary_key=True)
    descricao = models.CharField(max_length=450)
    obrigatorio = models.CharField(max_length=3000)
    desejavel = models.CharField(max_length=3000)
    responsabilidade = models.CharField(max_length=3000)
    ccusto = models.ForeignKey(Ccusto, models.DO_NOTHING, db_column='ccusto')
    
    def __str__(self):
        return self.descricao

    class Meta:
        db_table = 'cargo'


class Funcionario(models.Model):
    id_funcionario = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=150)
    sobrenome = models.CharField(max_length=150)
    cpf = models.CharField(max_length=11)
    telefone = models.CharField(max_length=14)
    ramal = models.CharField(max_length=5)
    ccusto = models.ForeignKey(Ccusto, models.DO_NOTHING, db_column='ccusto')

    def __str__(self):
        return '{} {}' . format(self.nome, self.sobrenome)

    def getNome(self):
        return '{} {}' . format(self.nome, self.sobrenome)

    class Meta:
        db_table = 'funcionario'


class Permissao(models.Model):
    id_permissao = models.AutoField(primary_key=True)
    descricao = models.CharField(max_length=150)

    class Meta:
        db_table = 'permissao'


class Sexo(models.Model):
    id_sexo = models.AutoField(primary_key=True)
    descricao = models.CharField(max_length=50)

    def __str__(self):
        return self.descricao

    class Meta:
        db_table = 'sexo'

class Tiporequisicao(models.Model):
    id_tiporequisicao = models.AutoField(primary_key=True)
    descricao = models.CharField(max_length=50)
    funcionario_substituicao = models.ForeignKey(Funcionario, models.DO_NOTHING, blank=True, null=True, db_column='funcionario_substituicao')

    def __str__(self):
        return self.descricao

    class Meta:
        db_table = 'tiporequisicao'


class Usuario(models.Model):
    id_usuario = models.AutoField(primary_key=True)
    email = models.CharField(max_length=100)
    senha = models.CharField(max_length=50)
    permissao = models.ForeignKey(Permissao, models.DO_NOTHING)
    funcionario = models.ForeignKey(Funcionario, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'usuario'

