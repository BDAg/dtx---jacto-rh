from django.contrib import admin

from pages.models import Permissao, Tipocontrato, Tiporequisicao, Ccusto, Funcionario, Cargo

class PermissaoAdmin(admin.ModelAdmin):
    list_display = ('id_permissao', 'descricao')
    list_display_links = ('id_permissao', 'descricao')
    list_filter = ('id_permissao', 'descricao')
    search_fields = ('id_permissao', 'descricao')
    list_per_page = 15

admin.site.register(Permissao, PermissaoAdmin)

class TipocontratoAdmin(admin.ModelAdmin):
    list_display = ('id_tipocontrato', 'descricao')
    list_display_links = ('id_tipocontrato', 'descricao')
    list_filter = ('id_tipocontrato', 'descricao')
    search_fields = ('id_tipocontrato', 'descricao')
    list_per_page = 15

admin.site.register(Tipocontrato, TipocontratoAdmin)

class CcustoAdmin(admin.ModelAdmin):
    list_display = ('id_ccusto', 'descricao')
    list_display_links = ('id_ccusto', 'descricao')
    list_filter = ('id_ccusto', 'descricao')
    search_fields = ('id_ccusto', 'descricao')
    list_per_page = 15

admin.site.register(Ccusto, CcustoAdmin)

class FuncionarioAdmin(admin.ModelAdmin):
    list_display = ('id_funcionario', 'getNome', 'cpf', 'telefone', 'ramal', 'ccusto')
    list_display_links = ('id_funcionario', 'getNome', 'cpf', 'telefone', 'ramal', 'ccusto')
    list_filter = ('id_funcionario', 'nome', 'sobrenome', 'cpf', 'telefone', 'ramal', 'ccusto')
    search_fields = ['id_funcionario', 'nome', 'sobrenome', 'telefone', 'ramal', 'ccusto__descricao']
    list_per_page = 15

admin.site.register(Funcionario, FuncionarioAdmin)

class TiporequisicaoAdmin(admin.ModelAdmin):
    list_display = ('id_tiporequisicao', 'descricao', 'funcionario_substituicao')
    list_display_links = ('id_tiporequisicao', 'descricao', 'funcionario_substituicao')
    list_filter = ('id_tiporequisicao', 'descricao', 'funcionario_substituicao')
    search_fields = ['id_tiporequisicao', 'descricao', 'funcionario_substituicao__nome', 'funcionario_substituicao__sobrenome']
    list_per_page = 15

admin.site.register(Tiporequisicao, TiporequisicaoAdmin)

class CargoAdmin(admin.ModelAdmin):
    list_display = ('id_cargo', 'descricao', 'obrigatorio', 'desejavel', 'responsabilidade', 'ccusto')
    list_display_links = ('id_cargo', 'descricao', 'obrigatorio', 'desejavel', 'responsabilidade', 'ccusto')
    list_filter = ('id_cargo', 'descricao', 'obrigatorio', 'desejavel', 'responsabilidade', 'ccusto')
    search_fields = ['id_cargo', 'descricao', 'obrigatorio', 'desejavel', 'responsabilidade', 'ccusto__descricao']
    list_per_page = 15 

admin.site.register(Cargo, CargoAdmin)